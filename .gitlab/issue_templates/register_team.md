**Guidelines**: In order to register your team for participation in this brainhack project fill the required information in this issue template and submit your issue.

Note: issue title should be `Team registeration: <team name>` (replace <team name> with your desired team name)

# Team registration:

**Team name**:

<!-- Choose a name for your team --> 



**Team members**:

<!-- Provide full names of all individuals in the team (comma separated). You may also provide additional information such as affiliations, or location (city, country) --> 

<!-- Teams can have up to 3 members --> 



**Team lead email**:

<!-- A contact information from the team lead (one of the members) for future communications --> 



**Team lead mattermost handle**: (optional)

<!-- Provide the team lead mattermost handle to be added to the project channel --> 



**Other social media handles**: (optional)

<!-- You may provide any optional social media handles here. (e.g. twitter, github, gitlab, etc.) --> 









